# Tuition Pay Off Calc

See if you should hold off on paying your loans!

Bernie Sanders may cancel students loans for all, so lets see if it makes sense to keep paying

Taking into account
- His chance to win
- The length of time you think loans will be forgiven at
- Your loan information

A simple average gain is calculated `AvgGain=a×p−b×q.` where `a` and  `p` are the chances of election and not election respectively, `p` is your original loan, and  `b` the amount you have left to pay after x months.

Basically, does the bet payoff of wiping away the original loan `p` make up for the chance of being left with your new loan amount `b`.

http://people.wku.edu/david.neal/109/Unit2/Payoffs.pdf

Don't actually use this for guidance but maybe think about it?