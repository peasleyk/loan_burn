
class Input {
  constructor(loan, apy, payment_amount, months, chance_to_win) {
    this._loan = parseFloat(loan)
    this._apy = parseFloat(apy)
    this._payment_amount = parseFloat(payment_amount)
    this._months = parseFloat(months)
    this._chance_to_win = parseFloat(chance_to_win)
    this._data = null
  }

  get loan(){
    return this._loan
  }

  get apy(){
    return this._apy
  }

  get payment_amount(){
    return this._payment_amount
  }

  get months(){
    return this._months
  }

  get chance_to_win(){
    return this._chance_to_win
  }

  get data(){
    return this._data
  }

  set data(data){
    this._data = data
  }

  calculate_average_gain(amount_left){
    let average_gain = (this.loan * this.chance_to_win) - ((amount_left)*(1-this.chance_to_win))
    return average_gain
  }


  compute_stats(){
    const daily_apy = this.apy / 365
    let interest = 0
    let amount_left = this.loan
    let actual_months = 0
    let monthly_interest = 0
    let months = []
    let average_gain_moving = []

    // Calculate core statistics
    while (actual_months < this.months) {
      actual_months++
      monthly_interest = (daily_apy * amount_left) * 30.42
      interest = interest + monthly_interest

      amount_left = amount_left - this.payment_amount + monthly_interest

      // For chart
      months.push(actual_months)
      average_gain_moving.push(this.calculate_average_gain(amount_left))

      if(amount_left <= 0) {
        break
      }
    }

    // In case too many months were entered
    let principle_paid = this.loan - amount_left

    // Create chart attribute
    this.data = {
      labels: months,
      series: [average_gain_moving]
    }

    // Average gain based on chance and amount paid
    let average_gain = this.calculate_average_gain(amount_left)
    let results = new Results(actual_months, amount_left, principle_paid, interest, average_gain)
    return results
  }

  update

}

class Results {
  constructor(actual_months, amount_left, principle_paid, interest, average_gain) {
    this._actual_months = parseInt(actual_months)
    this._amount_left = parseInt(amount_left)
    this._principle_paid = parseInt(principle_paid)
    this._interest = parseInt(interest)
    this._average_gain = parseInt(average_gain)
  }

  get actual_months(){
    return this._actual_months
  }

  get amount_left(){
    return this._amount_left
  }

  get principle_paid(){
    return this._principle_paid
  }

  get interest(){
    return this._interest
  }

  get average_gain(){
    return this._average_gain
  }

  updateOutput(){
    let payment_left_element = document.getElementById("payment_left")
    payment_left_element.innerHTML = `$${this._amount_left}`

    let months_left_element = document.getElementById("months_paid")
    months_left_element.innerHTML = `${this._actual_months}`


    let interest_element = document.getElementById("interest_paid")
    interest_element.innerHTML = `$${this._interest}`

    let principle_element = document.getElementById("principle_paid")
    principle_element.innerHTML = `$${this._principle_paid}`

    let average_gain_element = document.getElementById("average_gain")
    average_gain_element.innerHTML = `$${this._average_gain}`

    let verdict_element = document.getElementById("verdict")

    if (this._average_gain > 0){
      average_gain_element.setAttribute("style", "color: green")
      verdict_element.innerHTML = "Looks like you shouldn't pay any more towards your loan!"
    }
    else{
      average_gain_element.setAttribute("style", "color: red")
      verdict_element.innerHTML = "Ruh-Roh better start paying more"
      }
    }
}

function create_chart(data){
  new Chartist.Line('.ct-chart', data, {
    showArea: true,
    chartPadding: {
      left: 20,
      bottom: 20
    },
    plugins: [
      Chartist.plugins.ctThreshold({
        threshold: 4
      }),
    ]}
  )
}

function go(){
  let loan = document.getElementById("loan_input").value
  let apy = document.getElementById("apy_input").value
  let minpay = document.getElementById("minpay_input").value
  let months = document.getElementById("months_input").value
  let chance = document.getElementById("chance_input").value

  let i = new Input(loan, apy, minpay, months, chance)
  let results = i.compute_stats()
  results.updateOutput()
  create_chart(i.data)
}

let outputs = document.querySelectorAll(".output");
outputs.forEach(function(elem) {

    // Just so we have a blank chart
    create_chart()

    elem.addEventListener("input", function() {
      corresponding_element = elem.id.split("_")[0]
      slider = document.getElementById(corresponding_element)
      slider.value = elem.value
      go()
    });
});

let sliders = document.querySelectorAll(".slider");

sliders.forEach(function(elem) {
    elem.addEventListener("input", function() {
      box = document.getElementById(`${elem.id}_input`)
      box.value = elem.value
      go()
    });
});